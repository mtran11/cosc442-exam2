/*
 * 
 */
package game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class WorldBuilder.
 */
public class WorldBuilder {
	
	private WorldBuilderData data = new WorldBuilderData();

	/**
	 * Instantiates a new world builder.
	 *
	 * @param width the width
	 * @param height the height
	 * @param depth the depth
	 */
	public WorldBuilder(int width, int height, int depth) {
		this.data.width = width;
		this.data.height = height;
		this.data.depth = depth;
		this.data.tiles = new Tile[width][height][depth];
		this.data.regions = new int[width][height][depth];
		this.data.nextRegion = 1;
	}

	/**
	 * Builds the.
	 *
	 * @return the world
	 */
	public World build() {
		return new World(data.tiles);
	}

	/**
	 * Randomize tiles.
	 *
	 * @return the world builder
	 */
	private WorldBuilder randomizeTiles() {
		for (int x = 0; x < data.width; x++) {
			for (int y = 0; y < data.height; y++) {
				for (int z = 0; z < data.depth; z++) {
					data.tiles[x][y][z] = Math.random() < 0.5 ? Tile.FLOOR : Tile.WALL;
				}
			}
		}
		return this;
	}

	/**
	 * Smooth.
	 *
	 * @param times the times
	 * @return the world builder
	 */
	private WorldBuilder smooth(int times) {
		Tile[][][] tiles2 = new Tile[data.width][data.height][data.depth];
		for (int time = 0; time < times; time++) {

			data.smoothExtracted(tiles2);
			data.tiles = tiles2;
		}
		return this;
	}

	/**
	 * Creates the regions.
	 *
	 * @return the world builder
	 */
	private WorldBuilder createRegions(){
		data.regions = new int[data.width][data.height][data.depth];
		
		for (int z = 0; z < data.depth; z++){
			for (int x = 0; x < data.width; x++){
				for (int y = 0; y < data.height; y++){
					if (data.tiles[x][y][z] != Tile.WALL && data.regions[x][y][z] == 0){
						int size = data.fillRegion(data.nextRegion++, x, y, z);
						
						if (size < 25)
							data.removeRegion(data.nextRegion - 1, z);
					}
				}
			}
		}
		return this;
	}
	
	/**
	 * Connect regions.
	 *
	 * @return the world builder
	 */
	public WorldBuilder connectRegions(){
		for (int z = 0; z < data.depth-1; z++){
			data.connectRegionsDown(z, this);
		}
		return this;
	}
	
	/**
	 * Connect regions down.
	 *
	 * @param z the z
	 * @param r1 the r 1
	 * @param r2 the r 2
	 */
	public void connectRegionsDown(int z, int r1, int r2){
		data.connectRegionsDown(z, r1, r2);
	}

	/**
	 * Adds the exit stairs.
	 *
	 * @return the world builder
	 */
	private WorldBuilder addExitStairs() {
		int x = -1;
		int y = -1;
		
		do {
			x = (int)(Math.random() * data.width);
			y = (int)(Math.random() * data.height);
		}
		while (data.tiles[x][y][0] != Tile.FLOOR);
		
		data.tiles[x][y][0] = Tile.STAIRS_UP;
		return this;
	}

	/**
	 * Make caves.
	 *
	 * @return the world builder
	 */
	public WorldBuilder makeCaves() {
		return randomizeTiles()
				.smooth(8)
				.createRegions()
				.connectRegions()
				.addExitStairs();
	}
}