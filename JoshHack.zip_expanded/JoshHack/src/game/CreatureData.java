package game;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class CreatureData {
	/** The world. */
	public World world;
	/** The x. */
	public int x;
	/** The y. */
	public int y;
	/** The z. */
	public int z;
	/** The glyph. */
	public char glyph;
	/** The color. */
	public Color color;
	/** The ai. */
	public CreatureAi ai;
	/** The max hp. */
	public int maxHp;
	/** The hp. */
	public int hp;
	/** The attack value. */
	public int attackValue;
	/** The defense value. */
	public int defenseValue;
	/** The vision radius. */
	public int visionRadius;
	/** The name. */
	public String name;
	/** The inventory. */
	public Inventory inventory;
	/** The max food. */
	public int maxFood;
	/** The food. */
	public int food;
	/** The weapon. */
	public Item weapon;
	/** The armor. */
	public Item armor;
	/** The xp. */
	public int xp;
	/** The level. */
	public int level;
	/** The regen hp cooldown. */
	public int regenHpCooldown;
	/** The regen hp per 1000. */
	public int regenHpPer1000;
	/** The effects. */
	public List<Effect> effects;
	/** The max mana. */
	public int maxMana;
	/** The mana. */
	public int mana;
	/** The regen mana cooldown. */
	public int regenManaCooldown;
	/** The regen mana per 1000. */
	public int regenManaPer1000;
	/** The cause of death. */
	public String causeOfDeath;
	/** The detect creatures. */
	public int detectCreatures;

	public CreatureData() {
	}

	/**
	 * Attack value.
	 *
	 * @param creature TODO
	 * @return the int
	 */
	public int attackValue(Creature creature) { 
		return attackValue
			+ (weapon == null ? 0 : weapon.attackValue())
			+ (armor == null ? 0 : armor.attackValue());
	}

	/**
	 * Defense value.
	 *
	 * @param creature TODO
	 * @return the int
	 */
	public int defenseValue(Creature creature) { 
		return defenseValue
			+ (weapon == null ? 0 : weapon.defenseValue())
			+ (armor == null ? 0 : armor.defenseValue());
	}

	public Point extracted(int wx, int wy, Point end, Creature creature) {
		for (Point p : new Line(this.x, this.y, wx, wy)) {
			if (!creature.realTile(p.x, p.y, this.z).isGround())
				break;
			end = p;
		}
		return end;
	}

	/**
	 * Gain xp.
	 * @param other  the other
	 * @param creature
	 */
	public void gainXp(Creature other, Creature creature) {
		int amount = other.data.maxHp + other.data.attackValue(other) + other.data.defenseValue(other) - this.level;
		if (amount > 0)
			creature.modifyXp(amount);
	}

	/**
	 * Leave corpse.
	 * @param creature
	 */
	public void leaveCorpse(Creature creature) {
		Item corpse = new Item('%', this.color, this.name + " corpse", null);
		corpse.modifyFoodValue(this.maxHp * 5);
		this.world.addAtEmptySpace(corpse, this.x, this.y, this.z);
		creature.inventory2();
	}

	/**
	 * Modify mana.
	 * @param amount  the amount
	 */
	public void modifyMana(int amount) {
		this.mana = Math.max(0, Math.min(this.mana + amount, this.maxMana));
	}

	/**
	 * Regenerate mana.
	 * @param creature
	 */
	public void regenerateMana(Creature creature) {
		this.regenManaCooldown -= this.regenManaPer1000;
		if (this.regenManaCooldown < 0) {
			if (this.mana < this.maxMana) {
				modifyMana(1);
				creature.modifyFood(-1);
			}
			this.regenManaCooldown += 1000;
		}
	}

	/**
	 * Gets the creatures who see me.
	 * @return  the creatures who see me
	 */
	public List<Creature> getCreaturesWhoSeeMe() {
		List<Creature> others = new ArrayList<Creature>();
		int r = 9;
		for (int ox = -r; ox < r + 1; ox++) {
			for (int oy = -r; oy < r + 1; oy++) {
				if (ox * ox + oy * oy > r * r)
					continue;
				Creature other = this.world.creature(this.x + ox, this.y + oy, this.z);
				if (other == null)
					continue;
				others.add(other);
			}
		}
		return others;
	}
}