/*
 * 
 */
package game;

import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class CreatureAi.
 */
public class CreatureAi {
	
	protected CreatureAiData data = new CreatureAiData();

	/**
	 * Instantiates a new creature ai.
	 *
	 * @param creature the creature
	 */
	public CreatureAi(Creature creature){
		this.data.creature = creature;
		this.data.creature.setCreatureAi(this);
		this.data.itemNames = new HashMap<String, String>();
	}
	
	/**
	 * Gets the name.
	 *
	 * @param item the item
	 * @return the name
	 */
	public String getName(Item item){
		String name = data.itemNames.get(item.name());
		
		return name == null ? item.appearance() : name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param item the item
	 * @param name the name
	 */
	public void setName(Item item, String name){
		data.itemNames.put(item.name(), name);
	}
	
	/**
	 * On enter.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param tile the tile
	 */
	public void onEnter(int x, int y, int z, Tile tile){
		if (tile.isGround()){
			onEnterExtracted(x, y, z);
		} else {
			data.creature.doAction("bump into a wall");
		}
	}

	private void onEnterExtracted(int x, int y, int z) {
		data.creature.data.x = x;
		data.creature.data.y = y;
		data.creature.data.z = z;
	}
	
	/**
	 * On update.
	 */
	public void onUpdate(){
	}
	
	/**
	 * On notify.
	 *
	 * @param message the message
	 */
	public void onNotify(String message){
	}

	/**
	 * Can see.
	 *
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 * @return true, if successful
	 */
	public boolean canSee(int wx, int wy, int wz) {
		if (data.creature.data.z != wz)
			return false;
		
		if (canSeeExtracted(wx, wy))
			return false;
		
		for (Point p : new Line(data.creature.data.x, data.creature.data.y, wx, wy)){
			if (data.creature.realTile(p.x, p.y, wz).isGround() || p.x == wx && p.y == wy)
				continue;
			
			return false;
		}
		
		return true;
	}

	private boolean canSeeExtracted(int wx, int wy) {
		return (data.creature.data.x-wx)*(data.creature.data.x-wx) + (data.creature.data.y-wy)*(data.creature.data.y-wy) > data.creature.visionRadius()*data.creature.visionRadius();
	}
	
	/**
	 * Wander.
	 */
	public void wander(){
		int mx = (int)(Math.random() * 3) - 1;
		int my = (int)(Math.random() * 3) - 1;
		
		Creature other = data.creature.creature(data.creature.data.x + mx, data.creature.data.y + my, data.creature.data.z);
		
		wanderExtracted(mx, my, other);
	}

	private void wanderExtracted(int mx, int my, Creature other) {
		if (other != null && other.name().equals(data.creature.name()) 
				|| !data.creature.tile(data.creature.data.x+mx, data.creature.data.y+my, data.creature.data.z).isGround())
			return;
		else
			data.creature.moveBy(mx, my, 0);
	}

	/**
	 * On gain level.
	 */
	public void onGainLevel() {
		new LevelUpController().autoLevelUp(data.creature);
	}

	/**
	 * Remembered tile.
	 *
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 * @return the tile
	 */
	public Tile rememberedTile(int wx, int wy, int wz) {
		return Tile.UNKNOWN;
	}

	/**
	 * Can throw at.
	 *
	 * @param other the other
	 * @return true, if successful
	 */
	protected boolean canThrowAt(Creature other) {
		return data.creature.canSee(other.data.x, other.data.y, other.data.z)
			&& getWeaponToThrow() != null;
	}

	/**
	 * Gets the weapon to throw.
	 *
	 * @return the weapon to throw
	 */
	protected Item getWeaponToThrow() {
		Item toThrow = null;
		
		for (Item item : data.creature.inventory().getItems()){
			if (item == null || data.creature.weapon() == item || data.creature.armor() == item)
				continue;
			
			if (toThrow == null || item.thrownAttackValue() > toThrow.attackValue())
				toThrow = item;
		}
		
		return toThrow;
	}

	/**
	 * Can ranged weapon attack.
	 *
	 * @param other the other
	 * @return true, if successful
	 */
	protected boolean canRangedWeaponAttack(Creature other) {
		return data.creature.weapon() != null
		    && data.creature.weapon().rangedAttackValue() > 0
		    && data.creature.canSee(other.data.x, other.data.y, other.data.z);
	}

	/**
	 * Can pickup.
	 *
	 * @return true, if successful
	 */
	protected boolean canPickup() {
		return data.creature.item(data.creature.data.x, data.creature.data.y, data.creature.data.z) != null
			&& !data.creature.inventory().isFull();
	}

	/**
	 * Hunt.
	 *
	 * @param target the target
	 */
	public void hunt(Creature target) {
		List<Point> points = new Path(data.creature, target.data.x, target.data.y).points();
		
		int mx = points.get(0).x - data.creature.data.x;
		int my = points.get(0).y - data.creature.data.y;
		
		data.creature.moveBy(mx, my, 0);
	}

	/**
	 * Can use better equipment.
	 *
	 * @return true, if successful
	 */
	protected boolean canUseBetterEquipment() {
		int currentWeaponRating = data.creature.weapon() == null ? 0 : data.creature.weapon().attackValue() + data.creature.weapon().rangedAttackValue();
		int currentArmorRating = data.creature.armor() == null ? 0 : data.creature.armor().defenseValue();
		
		for (Item item : data.creature.inventory().getItems()){
			if (item == null)
				continue;
			
			boolean isArmor = item.attackValue() + item.rangedAttackValue() < item.defenseValue();
			
			if (item.attackValue() + item.rangedAttackValue() > currentWeaponRating
					|| isArmor && item.defenseValue() > currentArmorRating)
				return true;
		}
		
		return false;
	}

	/**
	 * Use better equipment.
	 */
	protected void useBetterEquipment() {
		int currentWeaponRating = data.creature.weapon() == null ? 0 : data.creature.weapon().attackValue() + data.creature.weapon().rangedAttackValue();
		int currentArmorRating = data.creature.armor() == null ? 0 : data.creature.armor().defenseValue();
		
		useBetterEquipmentExtracted(currentWeaponRating, currentArmorRating);
	}

	private void useBetterEquipmentExtracted(int currentWeaponRating, int currentArmorRating) {
		for (Item item : data.creature.inventory().getItems()){
			if (item == null)
				continue;
			
			boolean isArmor = item.attackValue() + item.rangedAttackValue() < item.defenseValue();
			
			useBEExtracted(currentWeaponRating, currentArmorRating, item, isArmor);
		}
	}

	private void useBEExtracted(int currentWeaponRating, int currentArmorRating, Item item,
			boolean isArmor) {
		if (item.attackValue() + item.rangedAttackValue() > currentWeaponRating
				|| isArmor && item.defenseValue() > currentArmorRating) {
			data.creature.equip(item);
		}
	}
}
