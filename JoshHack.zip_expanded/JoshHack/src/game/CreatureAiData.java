package game;

import java.util.Map;

public class CreatureAiData {
	/** The creature. */
	public Creature creature;
	/** The item names. */
	public Map<String, String> itemNames;

	public CreatureAiData() {
	}
}