/*
 * 
 */
package game;


// TODO: Auto-generated Javadoc
/**
 * The Class ZombieAi.
 */
public class ZombieAi extends CreatureAi {
	
	/** The player. */
	private Creature player;
	
	/**
	 * Instantiates a new zombie ai.
	 *
	 * @param creature the creature
	 * @param player the player
	 */
	public ZombieAi(Creature creature, Creature player) {
		super(creature);
		this.player = player;
	}

	/**
	 * On update.
	 */
	public void onUpdate(){
		if (Math.random() < 0.2)
			return;
		
		if (data.creature.canSee(player.data.x, player.data.y, player.data.z))
			hunt(player);
		else
			wander();
	}
}
