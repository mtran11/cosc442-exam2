/*
 * 
 */
package game;

// TODO: Auto-generated Javadoc
/**
 * The Class FungusAi.
 */
public class FungusAi extends CreatureAi {
	
	/** The factory. */
	private StuffFactory factory;
	
	/** The spreadcount. */
	private int spreadcount;
	
	/**
	 * Instantiates a new fungus ai.
	 *
	 * @param creature the creature
	 * @param factory the factory
	 */
	public FungusAi(Creature creature, StuffFactory factory) {
		super(creature);
		this.factory = factory;
	}

	/**
	 * On update.
	 */
	public void onUpdate(){
		if (spreadcount < 5 && Math.random() < 0.01)
			spread();
	}
	
	/**
	 * Spread.
	 */
	private void spread(){
		int x = data.creature.data.x + (int)(Math.random() * 11) - 5;
		int y = data.creature.data.y + (int)(Math.random() * 11) - 5;
		
		if (!data.creature.canEnter(x, y, data.creature.data.z))
			return;
		
		data.creature.doAction("spawn a child");
		
		Creature child = factory.newFungus(data.creature.data.z);
		child.data.x = x;
		child.data.y = y;
		child.data.z = data.creature.data.z;
		spreadcount++;
	}
}
