/*
 * 
 */
package game;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Class Creature.
 */
public class Creature {
	
	/**
	 * Glyph.
	 *
	 * @return the char
	 */
	public char glyph() { return data.glyph; }
	
	/**
	 * Color.
	 *
	 * @return the color
	 */
	public Color color() { return data.color; }

	/**
	 * Sets the creature ai.
	 *
	 * @param ai the new creature ai
	 */
	public void setCreatureAi(CreatureAi ai) { this.data.ai = ai; }
	
	/**
	 * Max hp.
	 *
	 * @return the int
	 */
	public int maxHp() { return data.maxHp; }
	
	/**
	 * Modify max hp.
	 *
	 * @param amount the amount
	 */
	public void modifyMaxHp(int amount) { data.maxHp += amount; }
	
	/**
	 * Hp.
	 *
	 * @return the int
	 */
	public int hp() { return data.hp; }
	
	/**
	 * Modify attack value.
	 *
	 * @param value the value
	 */
	public void modifyAttackValue(int value) { data.attackValue += value; }
	
	/**
	 * Modify defense value.
	 *
	 * @param value the value
	 */
	public void modifyDefenseValue(int value) { data.defenseValue += value; }
	
	/**
	 * Modify vision radius.
	 *
	 * @param value the value
	 */
	public void modifyVisionRadius(int value) { data.visionRadius += value; }
	
	/**
	 * Vision radius.
	 *
	 * @return the int
	 */
	public int visionRadius() { return data.visionRadius; }

	/**
	 * Name.
	 *
	 * @return the string
	 */
	public String name() { return data.name; }

	/**
	 * Inventory.
	 *
	 * @return the inventory
	 */
	public Inventory inventory() { return data.inventory; }

	/**
	 * Max food.
	 *
	 * @return the int
	 */
	public int maxFood() { return data.maxFood; }
	
	/**
	 * Food.
	 *
	 * @return the int
	 */
	public int food() { return data.food; }
	
	/**
	 * Weapon.
	 *
	 * @return the item
	 */
	public Item weapon() { return data.weapon; }
	
	/**
	 * Armor.
	 *
	 * @return the item
	 */
	public Item armor() { return data.armor; }
	
	/**
	 * Xp.
	 *
	 * @return the int
	 */
	public int xp() { return data.xp; }
	
	/**
	 * Modify xp.
	 *
	 * @param amount the amount
	 */
	public void modifyXp(int amount) { 
		data.xp += amount;
		
		notify("You %s %d xp.", amount < 0 ? "lose" : "gain", amount);
		
		while (data.xp > (int)(Math.pow(data.level, 1.75) * 25)) {
			data.level++;
			doAction("advance to level %d", data.level);
			data.ai.onGainLevel();
			modifyHp(data.level * 2, "Died from having a negative level?");
		}
	}
	
	/**
	 * Level.
	 *
	 * @return the int
	 */
	public int level() { return data.level; }
	
	/**
	 * Modify regen hp per 1000.
	 *
	 * @param amount the amount
	 */
	public void modifyRegenHpPer1000(int amount) { data.regenHpPer1000 += amount; }
	
	/**
	 * Effects.
	 *
	 * @return the list
	 */
	public List<Effect> effects(){ return data.effects; }
	
	/**
	 * Max mana.
	 *
	 * @return the int
	 */
	public int maxMana() { return data.maxMana; }
	
	/**
	 * Modify max mana.
	 *
	 * @param amount the amount
	 */
	public void modifyMaxMana(int amount) { data.maxMana += amount; }
	
	/**
	 * Mana.
	 *
	 * @return the int
	 */
	public int mana() { return data.mana; }
	
	/**
	 * Modify mana.
	 *
	 * @param amount the amount
	 */
	public void modifyMana(int amount) { data.modifyMana(amount); }
	
	/**
	 * Modify regen mana per 1000.
	 *
	 * @param amount the amount
	 */
	public void modifyRegenManaPer1000(int amount) { data.regenManaPer1000 += amount; }
	
	/**
	 * Cause of death.
	 *
	 * @return the string
	 */
	public String causeOfDeath() { return data.causeOfDeath; }
	
	/**
	 * Instantiates a new creature.
	 *
	 * @param world the world
	 * @param glyph the glyph
	 * @param color the color
	 * @param name the name
	 * @param maxHp the max hp
	 * @param attack the attack
	 * @param defense the defense
	 */
	public Creature(World world, char glyph, Color color, String name, int maxHp, int attack, int defense){
		this.data.world = world;
		this.data.glyph = glyph;
		this.data.color = color;
		this.data.maxHp = maxHp;
		this.data.hp = maxHp;
		this.data.attackValue = attack;
		this.data.defenseValue = defense;
		this.data.visionRadius = 9;
		this.data.name = name;
		this.data.inventory = new Inventory(20);
		this.data.maxFood = 1000;
		this.data.food = data.maxFood / 3 * 2;
		this.data.level = 1;
		this.data.regenHpPer1000 = 10;
		this.data.effects = new ArrayList<Effect>();
		this.data.maxMana = 5;
		this.data.mana = data.maxMana;
		this.data.regenManaPer1000 = 20;
	}
	
	/**
	 * Move by.
	 *
	 * @param mx the mx
	 * @param my the my
	 * @param mz the mz
	 */
	public void moveBy(int mx, int my, int mz){
		if (mx==0 && my==0 && mz==0)
			return;
		
		Tile tile = data.world.tile(data.x+mx, data.y+my, data.z+mz);
		
		if (mz == -1){
			if (tile == Tile.STAIRS_DOWN) {
				doAction("walk up the stairs to level %d", data.z+mz+1);
			} else {
				doAction("try to go up but are stopped by the cave ceiling");
				return;
			}
		} else if (mz == 1){
			if (tile == Tile.STAIRS_UP) {
				doAction("walk down the stairs to level %d", data.z+mz+1);
			} else {
				doAction("try to go down but are stopped by the cave floor");
				return;
			}
		}
		
		Creature other = other(mx, my, mz);
		if (other == null)
			data.ai.onEnter(data.x+mx, data.y+my, data.z+mz, tile);
	}

	/**
	 * Other.
	 *
	 * @param mx the mx
	 * @param my the my
	 * @param mz the mz
	 * @return the creature
	 */
	private Creature other(int mx, int my, int mz) {
		Creature other = data.world.creature(data.x + mx, data.y + my, data.z + mz);
		modifyFood(-1);
		if (other == null) {
		} else
			meleeAttack(other);
		return other;
	}

	/**
	 * Melee attack.
	 *
	 * @param other the other
	 */
	public void meleeAttack(Creature other){
		commonAttack(other, data.attackValue(this), "attack the %s for %d damage", other.data.name);
	}

	/**
	 * Throw attack.
	 *
	 * @param item the item
	 * @param other the other
	 */
	private void throwAttack(Item item, Creature other) {
		commonAttack(other, data.attackValue / 2 + item.thrownAttackValue(), "throw a %s at the %s for %d damage", nameOf(item), other.data.name);
		other.addEffect(item.quaffEffect());
	}
	
	/**
	 * Ranged weapon attack.
	 *
	 * @param other the other
	 */
	public void rangedWeaponAttack(Creature other){
		commonAttack(other, data.attackValue / 2 + data.weapon.rangedAttackValue(), "fire a %s at the %s for %d damage", nameOf(data.weapon), other.data.name);
	}
	
	/**
	 * Common attack.
	 *
	 * @param other the other
	 * @param attack the attack
	 * @param action the action
	 * @param params the params
	 */
	private void commonAttack(Creature other, int attack, String action, Object ... params) {
		modifyFood(-2);
		
		int amount = Math.max(0, attack - other.data.defenseValue(other));
		
		amount = (int)(Math.random() * amount) + 1;
		
		Object[] params2 = new Object[params.length+1];
		for (int i = 0; i < params.length; i++){
			//params2[i] = params[i];
			System.arraycopy(params, i, params2, i, 1);
		}
		params2[params2.length - 1] = amount;
		
		doAction(action, params2);
		
		other.modifyHp(-amount, "Killed by a " + data.name);
		
		if (other.data.hp < 1)
			data.gainXp(other, this);
	}
	
	/**
	 * Modify hp.
	 *
	 * @param amount the amount
	 * @param causeOfDeath the cause of death
	 */
	public void modifyHp(int amount, String causeOfDeath) { 
		data.hp += amount;
		this.data.causeOfDeath = causeOfDeath;
		
		if (data.hp > data.maxHp) {
			data.hp = data.maxHp;
		} else if (data.hp < 1) {
			doAction("die");
			data.leaveCorpse(this);
			data.world.remove(this);
		}
	}
	
	/**
	 * Inventory 2.
	 */
	public void inventory2() {
		for (Item item : data.inventory.getItems()) {
			inventory3(item);
		}
	}

	/**
	 * Inventory 3.
	 *
	 * @param item the item
	 */
	private void inventory3(Item item) {
		if (item != null)
			drop(item);
	}
	
	/**
	 * Dig.
	 *
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 */
	public void dig(int wx, int wy, int wz) {
		modifyFood(-10);
		data.world.dig(wx, wy, wz);
		doAction("dig");
	}
	
	/**
	 * Update.
	 */
	public void update(){
		modifyFood(-1);
		regenerateHealth();
		data.regenerateMana(this);
		updateEffects();
		data.ai.onUpdate();
	}
	
	/**
	 * Update effects.
	 */
	private void updateEffects(){
		List<Effect> done = new ArrayList<Effect>();
		
		for (Effect effect : data.effects){
			effect.update(this);
			if (effect.isDone()) {
				effect.end(this);
				done.add(effect);
			}
		}
		
		data.effects.removeAll(done);
	}
	
	/**
	 * Regenerate health.
	 */
	private void regenerateHealth(){
		data.regenHpCooldown -= data.regenHpPer1000;
		if (data.regenHpCooldown < 0){
			if (data.hp < data.maxHp){
				modifyHp(1, "Died from regenerating health?");
				modifyFood(-1);
			}
			data.regenHpCooldown += 1000;
		}
	}

	/**
	 * Can enter.
	 *
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 * @return true, if successful
	 */
	public boolean canEnter(int wx, int wy, int wz) {
		return data.world.canEnter(wx, wy, wz);
	}

	/**
	 * Notify.
	 *
	 * @param message the message
	 * @param params the params
	 */
	public void notify(String message, Object ... params){
		data.ai.onNotify(String.format(message, params));
	}
	
	/**
	 * Do action.
	 *
	 * @param message the message
	 * @param params the params
	 */
	public void doAction(String message, Object ... params){
		for (Creature other : data.getCreaturesWhoSeeMe()){
			if (other == this){
				other.notify("You " + message + ".", params);
			} else {
				other.notify(String.format("The %s %s.", data.name, makeSecondPerson(message)), params);
			}
		}
	}
	
	/**
	 * Do action.
	 *
	 * @param item the item
	 * @param message the message
	 * @param params the params
	 */
	public void doAction(Item item, String message, Object ... params){
		if (data.hp < 1)
			return;
		
		doActionExtracted(item, message, params);
	}

	private void doActionExtracted(Item item, String message, Object... params) {
		for (Creature other : data.getCreaturesWhoSeeMe()){
			if (other == this){
				other.notify("You " + message + ".", params);
			} else {
				other.notify(String.format("The %s %s.", data.name, makeSecondPerson(message)), params);
			}
			other.learnName(item);
		}
	}
	
	/**
	 * Make second person.
	 *
	 * @param text the text
	 * @return the string
	 */
	private String makeSecondPerson(String text){
		String[] words = text.split(" ");
		words[0] = words[0] + "s";
		
		StringBuilder builder = new StringBuilder();
		for (String word : words){
			builder.append(" ");
			builder.append(word);
		}
		
		return builder.toString().trim();
	}
	
	/**
	 * Can see.
	 *
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 * @return true, if successful
	 */
	public boolean canSee(int wx, int wy, int wz){
		return (data.detectCreatures > 0 && data.world.creature(wx, wy, wz) != null
				|| data.ai.canSee(wx, wy, wz));
	}

	/**
	 * Real tile.
	 *
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 * @return the tile
	 */
	public Tile realTile(int wx, int wy, int wz) {
		return data.world.tile(wx, wy, wz);
	}
	
	/**
	 * Tile.
	 *
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 * @return the tile
	 */
	public Tile tile(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return data.world.tile(wx, wy, wz);
		else
			return data.ai.rememberedTile(wx, wy, wz);
	}

	/**
	 * Creature.
	 *
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 * @return the creature
	 */
	public Creature creature(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return data.world.creature(wx, wy, wz);
		else
			return null;
	}
	
	/**
	 * Pickup.
	 */
	public void pickup(){
		Item item = data.world.item(data.x, data.y, data.z);
		
		if (data.inventory.isFull() || item == null){
			doAction("grab at the ground");
		} else {
			doAction("pickup a %s", nameOf(item));
			data.world.remove(data.x, data.y, data.z);
			data.inventory.add(item);
		}
	}
	
	/**
	 * Drop.
	 *
	 * @param item the item
	 */
	public void drop(Item item){
		data(item);
		if (data.world.addAtEmptySpace(item, data.x, data.y, data.z)){
		} else {
			notify("There's nowhere to drop the %s.", nameOf(item));
		}
	}

	private void data(Item item) {
		if (data.world.addAtEmptySpace(item, data.x, data.y, data.z)) {
			dropSupport(item);
		} else {
		}
	}

	/**
	 * Drop support.
	 *
	 * @param item the item
	 */
	private void dropSupport(Item item) {
		doAction("drop a " + nameOf(item));
		data.inventory.remove(item);
		unequip(item);
	}
	
	/**
	 * Modify food.
	 *
	 * @param amount the amount
	 */
	public void modifyFood(int amount) { 
		data.food += amount;
		
		if (data.food > data.maxFood) {
			data.maxFood = (data.maxFood + data.food) / 2;
			data.food = data.maxFood;
			notify("You can't belive your stomach can hold that much!");
			modifyHp(-1, "Killed by overeating.");
		} else if (data.food < 1 && isPlayer()) {
			modifyHp(-1000, "Starved to death.");
		}
	}
	
	/**
	 * Checks if is player.
	 *
	 * @return true, if is player
	 */
	public boolean isPlayer(){
		return data.glyph == '@';
	}
	
	/**
	 * Eat.
	 *
	 * @param item the item
	 */
	public void eat(Item item){
		doAction("eat a " + nameOf(item));
		consume(item);
	}
	
	/**
	 * Quaff.
	 *
	 * @param item the item
	 */
	public void quaff(Item item){
		doAction("quaff a " + nameOf(item));
		consume(item);
	}
	
	/**
	 * Consume.
	 *
	 * @param item the item
	 */
	private void consume(Item item){
		if (item.foodValue() < 0)
			notify("Gross!");
		
		addEffect(item.quaffEffect());
		
		modifyFood(item.foodValue());
		getRidOf(item);
	}
	
	/**
	 * Adds the effect.
	 *
	 * @param effect the effect
	 */
	private void addEffect(Effect effect){
		if (effect == null)
			return;
		
		effect.start(this);
		data.effects.add(effect);
	}
	
	/**
	 * Gets the rid of.
	 *
	 * @param item the item
	 * @return the rid of
	 */
	private void getRidOf(Item item){
		data.inventory.remove(item);
		unequip(item);
	}
	
	/**
	 * Put at.
	 *
	 * @param item the item
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 */
	private void putAt(Item item, int wx, int wy, int wz){
		data.inventory.remove(item);
		unequip(item);
		data.world.addAtEmptySpace(item, wx, wy, wz);
	}
	
	/**
	 * Unequip.
	 *
	 * @param item the item
	 */
	public void unequip(Item item){
		if (item == null)
			return;
		
		if (item == data.armor){
			if (data.hp > 0)
				doAction("remove a " + nameOf(item));
			data.armor = null;
		} else if (item == data.weapon) {
			if (data.hp > 0) 
				doAction("put away a " + nameOf(item));
			data.weapon = null;
		}
	}
	
	/**
	 * Equip.
	 *
	 * @param item the item
	 */
	public void equip(Item item){
		if (!data.inventory.contains(item)) {
			if (data.inventory.isFull()) {
				notify("Can't equip %s since you're holding too much stuff.", nameOf(item));
				return;
			} else {
				data.world.remove(item);
				data.inventory.add(item);
			}
		}
		
		if (item.attackValue() == 0 && item.rangedAttackValue() == 0 && item.defenseValue() == 0)
			return;
		
		if (item.attackValue() + item.rangedAttackValue() >= item.defenseValue()){
			unequip(data.weapon);
			doAction("wield a " + nameOf(item));
			data.weapon = item;
		} else {
			unequip(data.armor);
			doAction("put on a " + nameOf(item));
			data.armor = item;
		}
	}
	
	/**
	 * Item.
	 *
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 * @return the item
	 */
	public Item item(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return data.world.item(wx, wy, wz);
		else
			return null;
	}
	
	/**
	 * Details.
	 *
	 * @return the string
	 */
	public String details() {
		return String.format("  level:%d  attack:%d  defense:%d  hp:%d", data.level, data.attackValue(this), data.defenseValue(this), data.hp);
	}
	
	/**
	 * Throw item.
	 *
	 * @param item the item
	 * @param wx the wx
	 * @param wy the wy
	 * @param wz the wz
	 */
	public void throwItem(Item item, int wx, int wy, int wz) {
		Point end = new Point(data.x, data.y, 0);
		
		end = data.extracted(wx, wy, end, this);
		
		wx = end.x;
		wy = end.y;
		
		Creature c = creature(wx, wy, wz);
		
		extracted(item, wx, wy, wz, c);
	}

	private void extracted(Item item, int wx, int wy, int wz, Creature c) {
		if (c != null)
			throwAttack(item, c);				
		else
			doAction("throw a %s", nameOf(item));
		
		if (item.quaffEffect() != null && c != null)
			getRidOf(item);
		else
			putAt(item, wx, wy, wz);
	}

	/**
	 * Summon.
	 *
	 * @param other the other
	 */
	public void summon(Creature other) {
		data.world.add(other);
	}
	
	public CreatureData data = new CreatureData();

	/**
	 * Modify detect creatures.
	 *
	 * @param amount the amount
	 */
	public void modifyDetectCreatures(int amount) { data.detectCreatures += amount; }
	
	/**
	 * Cast spell.
	 *
	 * @param spell the spell
	 * @param x2 the x 2
	 * @param y2 the y 2
	 */
	public void castSpell(Spell spell, int x2, int y2) {
		Creature other = creature(x2, y2, data.z);
		
		if (spell.manaCost() > data.mana){
			doAction("point and mumble but nothing happens");
			return;
		} else if (other == null) {
			doAction("point and mumble at nothing");
			return;
		}
		
		other.addEffect(spell.effect());
		data.modifyMana(-spell.manaCost());
	}
	
	/**
	 * Name of.
	 *
	 * @param item the item
	 * @return the string
	 */
	public String nameOf(Item item){
		return data.ai.getName(item);
	}
	
	/**
	 * Learn name.
	 *
	 * @param item the item
	 */
	public void learnName(Item item){
		notify("The " + item.appearance() + " is a " + item.name() + "!");
		data.ai.setName(item, item.name());
	}
}
