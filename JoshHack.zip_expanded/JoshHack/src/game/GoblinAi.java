/*
 * 
 */
package game;


// TODO: Auto-generated Javadoc
/**
 * The Class GoblinAi.
 */
public class GoblinAi extends CreatureAi {
	
	/** The player. */
	private Creature player;
	
	/**
	 * Instantiates a new goblin ai.
	 *
	 * @param creature the creature
	 * @param player the player
	 */
	public GoblinAi(Creature creature, Creature player) {
		super(creature);
		this.player = player;
	}

	/**
	 * On update.
	 */
	public void onUpdate(){
		if (canUseBetterEquipment())			
			onUpdateExtracted();
		else if (canPickup())
			data.creature.pickup();
		else
			wander();
	}

	private void onUpdateExtracted() {
		useBetterEquipment();
		if (canRangedWeaponAttack(player))
			data.creature.rangedWeaponAttack(player);
		else if (canThrowAt(player))
			onUpdateExtracted2();
	}
	
	private void onUpdateExtracted2() {
		if (canThrowAt(player))
			data.creature.throwItem(getWeaponToThrow(), player.data.x, player.data.y, player.data.z);
		else if (data.creature.canSee(player.data.x, player.data.y, player.data.z))
			hunt(player);
	}
}
