package game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WorldBuilderData {
	/** The width. */
	public int width;
	/** The height. */
	public int height;
	/** The depth. */
	public int depth;
	/** The tiles. */
	public Tile[][][] tiles;
	/** The regions. */
	public int[][][] regions;
	/** The next region. */
	public int nextRegion;

	public WorldBuilderData() {
	}

	/**
	 * Removes the region.
	 * @param region  the region
	 * @param z  the z
	 */
	public void removeRegion(int region, int z) {
		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				if (this.regions[x][y][z] == region) {
					this.regions[x][y][z] = 0;
					this.tiles[x][y][z] = Tile.WALL;
				}
			}
		}
	}

	/**
	 * Find region overlaps.
	 * @param z  the z
	 * @param r1  the r 1
	 * @param r2  the r 2
	 * @return  the list
	 */
	public List<Point> findRegionOverlaps(int z, int r1, int r2) {
		ArrayList<Point> candidates = new ArrayList<Point>();
		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				if (this.tiles[x][y][z] == Tile.FLOOR && this.tiles[x][y][z + 1] == Tile.FLOOR
						&& this.regions[x][y][z] == r1 && this.regions[x][y][z + 1] == r2) {
					candidates.add(new Point(x, y, z));
				}
			}
		}
		Collections.shuffle(candidates);
		return candidates;
	}

	/**
	 * Connect regions down.
	 * @param z  the z
	 * @param worldBuilder
	 */
	public void connectRegionsDown(int z, WorldBuilder worldBuilder) {
		List<Integer> connected = new ArrayList<Integer>();
		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				int r = this.regions[x][y][z] * 1000 + this.regions[x][y][z + 1];
				if (this.tiles[x][y][z] == Tile.FLOOR && this.tiles[x][y][z + 1] == Tile.FLOOR
						&& !connected.contains(r)) {
					connected.add(r);
					worldBuilder.connectRegionsDown(z, this.regions[x][y][z], this.regions[x][y][z + 1]);
				}
			}
		}
	}

	/**
	 * Connect regions down.
	 * @param z  the z
	 * @param r1  the r 1
	 * @param r2  the r 2
	 */
	public void connectRegionsDown(int z, int r1, int r2) {
		List<Point> candidates = findRegionOverlaps(z, r1, r2);
		int stairs = 0;
		do {
			Point p = candidates.remove(0);
			this.tiles[p.x][p.y][z] = Tile.STAIRS_DOWN;
			this.tiles[p.x][p.y][z + 1] = Tile.STAIRS_UP;
			stairs++;
		} while (candidates.size() / stairs > 250);
	}

	/**
	 * Fill region.
	 * @param region  the region
	 * @param x  the x
	 * @param y  the y
	 * @param z  the z
	 * @return  the int
	 */
	public int fillRegion(int region, int x, int y, int z) {
		int size = 1;
		ArrayList<Point> open = new ArrayList<Point>();
		open.add(new Point(x, y, z));
		this.regions[x][y][z] = region;
		while (!open.isEmpty()) {
			Point p = open.remove(0);
			for (Point neighbor : p.neighbors8()) {
				if (neighbor.x < 0 || neighbor.y < 0 || neighbor.x >= this.width || neighbor.y >= this.height)
					continue;
				if (this.regions[neighbor.x][neighbor.y][neighbor.z] > 0
						|| this.tiles[neighbor.x][neighbor.y][neighbor.z] == Tile.WALL)
					continue;
				size++;
				this.regions[neighbor.x][neighbor.y][neighbor.z] = region;
				open.add(neighbor);
			}
		}
		return size;
	}

	public void smoothExtracted(Tile[][][] tiles2) {
		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				for (int z = 0; z < this.depth; z++) {
					int floors = 0;
					int rocks = 0;
					for (int ox = -1; ox < 2; ox++) {
						for (int oy = -1; oy < 2; oy++) {
							if (x + ox < 0 || x + ox >= this.width || y + oy < 0 || y + oy >= this.height)
								continue;
							if (this.tiles[x + ox][y + oy][z] == Tile.FLOOR)
								floors++;
							else
								rocks++;
						}
					}
					tiles2[x][y][z] = floors >= rocks ? Tile.FLOOR : Tile.WALL;
				}
			}
		}
	}
}