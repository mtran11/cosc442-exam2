/*
 * 
 */
package ui;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import asciiPanel.AsciiPanel;
import game.EatScreen;
import game.FieldOfView;
import game.Item;
import game.StuffFactory;
import game.WorldBuilder;

// TODO: Auto-generated Javadoc
/**
 * The Class PlayScreen.
 */
public class PlayScreen implements Screen {
	
	public PlayScreenData data = new PlayScreenData();

	/**
	 * Instantiates a new play screen.
	 */
	public PlayScreen(){
		data.screenWidth = 80;
		data.screenHeight = 23;
		data.messages = new ArrayList<String>();
		createWorld();
		data.fov = new FieldOfView(data.world);
		
		StuffFactory factory = new StuffFactory(data.world);
		factory.createCreatures(this);
		factory.createItems(this);
	}

	/**
	 * Creates the world.
	 */
	private void createWorld(){
		data.world = new WorldBuilder(90, 32, 5)
					.makeCaves()
					.build();
	}
	
	/**
	 * Display output.
	 *
	 * @param terminal the terminal
	 */
	@Override
	public void displayOutput(AsciiPanel terminal) {
		int left = data.getScrollX(this);
		int top = data.getScrollY(this); 
		
		data.displayTiles(this, terminal, left, top);
		data.displayMessages(terminal, data.messages);
		
		String stats = String.format(" %3d/%3d hp   %d/%d mana   %8s", data.player.hp(), data.player.maxHp(), data.player.mana(), data.player.maxMana(), hunger());
		terminal.write(stats, 1, 23);
		
		if (data.subscreen != null) {
			data.subscreen.displayOutput(terminal);			
		}
	}
	
	/**
	 * Hunger.
	 *
	 * @return the string
	 */
	private String hunger(){
		if (data.player.food() < data.player.maxFood() * 0.10) {
			return "Starving";			
		}
		else if (data.player.food() < data.player.maxFood() * 0.25) {
			return "Hungry";			
		}
		else if (data.player.food() > data.player.maxFood() * 0.90) {
			return "Stuffed";			
		}
		else if (data.player.food() > data.player.maxFood() * 0.75) {
			return "Full";			
		}
		else {			
			return "";
		}
	}

	/**
	 * Respond to user input.
	 *
	 * @param key the key
	 * @return the screen
	 */
	@Override
	public Screen respondToUserInput(KeyEvent key) {
		int level = data.player.level();
		
		if (data.subscreen != null) {
			data.subscreen = data.subscreen.respondToUserInput(key);
		} else {
			switch (key.getKeyCode()){
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_H: data.player.moveBy(-1, 0, 0); break;
			case KeyEvent.VK_RIGHT:
			case KeyEvent.VK_L: data.player.moveBy( 1, 0, 0); break;
			case KeyEvent.VK_UP:
			case KeyEvent.VK_K: data.player.moveBy( 0,-1, 0); break;
			case KeyEvent.VK_DOWN:
			case KeyEvent.VK_J: data.player.moveBy( 0, 1, 0); break;
			case KeyEvent.VK_Y: data.player.moveBy(-1,-1, 0); break;
			case KeyEvent.VK_U: data.player.moveBy( 1,-1, 0); break;
			case KeyEvent.VK_B: data.player.moveBy(-1, 1, 0); break;
			case KeyEvent.VK_N: data.player.moveBy( 1, 1, 0); break;
			case KeyEvent.VK_D: data.subscreen = new DropScreen(data.player); break;
			case KeyEvent.VK_E: data.subscreen = new EatScreen(data.player); break;
			case KeyEvent.VK_W: data.subscreen = new EquipScreen(data.player); break;
			case KeyEvent.VK_X: data.subscreen = new ExamineScreen(data.player); break;
			case KeyEvent.VK_SEMICOLON: data.subscreen = new LookScreen(data.player, "Looking", 
					data.player.data.x - data.getScrollX(this), 
					data.player.data.y - data.getScrollY(this)); break;
			case KeyEvent.VK_T: data.subscreen = new ThrowScreen(data.player,
					data.player.data.x - data.getScrollX(this), 
					data.player.data.y - data.getScrollY(this)); break;
			case KeyEvent.VK_F: 
				if (data.player.weapon() == null || data.player.weapon().rangedAttackValue() == 0)
					data.player.notify("You don't have a ranged weapon equiped.");
				else
					data.subscreen = new FireWeaponScreen(data.player,
						data.player.data.x - data.getScrollX(this), 
						data.player.data.y - data.getScrollY(this)); break;
			case KeyEvent.VK_Q: data.subscreen = new QuaffScreen(data.player); break;
			case KeyEvent.VK_R: data.subscreen = new ReadScreen(data.player,
						data.player.data.x - data.getScrollX(this), 
						data.player.data.y - data.getScrollY(this)); break;
			}
			
			switch (key.getKeyChar()){
			case 'g':
			case ',': data.player.pickup(); break;
			case '<': 
				if (data.userIsTryingToExit(this))
					return userExits();
				else
					data.player.moveBy( 0, 0, -1); break;
			case '>': data.player.moveBy( 0, 0, 1); break;
			case '?': data.subscreen = new HelpScreen(); break;
			}
		}

		if (data.player.level() > level) {
			data.subscreen = new LevelUpScreen(data.player, data.player.level() - level);			
		}
		
		if (data.subscreen == null) {
			data.world.update();			
		}
		
		if (data.player.hp() < 1) {
			return new LoseScreen(data.player);			
		}
		
		return this;
	}

	/**
	 * User exits.
	 *
	 * @return the screen
	 */
	private Screen userExits(){
		for (Item item : data.player.inventory().getItems()){
			if (item != null && item.name().equals("teddy bear")) {
				return new WinScreen();				
			}
		}
		data.player.modifyHp(0, "Died while cowardly fleeing the caves.");
		return new LoseScreen(data.player);
	}
}
