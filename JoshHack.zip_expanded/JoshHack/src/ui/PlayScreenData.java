package ui;

import java.awt.Color;
import java.util.List;
import asciiPanel.AsciiPanel;
import game.Creature;
import game.FieldOfView;
import game.Tile;
import game.World;

public class PlayScreenData {
	/** The world. */
	public World world;
	/** The player. */
	public Creature player;
	/** The screen width. */
	public int screenWidth;
	/** The screen height. */
	public int screenHeight;
	/** The messages. */
	public List<String> messages;
	/** The fov. */
	public FieldOfView fov;
	/** The subscreen. */
	public Screen subscreen;

	public PlayScreenData() {
	}

	/**
	 * Display messages.
	 * @param terminal  the terminal
	 * @param messages  the messages
	 */
	public void displayMessages(AsciiPanel terminal, List<String> messages) {
		int top = this.screenHeight - messages.size();
		for (int i = 0; i < messages.size(); i++) {
			terminal.writeCenter(messages.get(i), top + i);
		}
		if (this.subscreen == null)
			messages.clear();
	}

	/**
	 * Display tiles.
	 *
	 * @param playScreen TODO
	 * @param terminal the terminal
	 * @param left the left
	 * @param top the top
	 */
	void displayTiles(PlayScreen playScreen, AsciiPanel terminal, int left, int top) {
		displayTilesExtracted();
		
		for (int x = 0; x < screenWidth; x++){
			for (int y = 0; y < screenHeight; y++){
				displayTilesExtracted2(terminal, left, top, x, y);
			}
		}
	}
	
	private void displayTilesExtracted() {
		fov.update(player.data.x, player.data.y, player.data.z, player.visionRadius());
	}

	private void displayTilesExtracted2(AsciiPanel terminal, int left, int top, int x, int y) {
		int wx = x + left;
		int wy = y + top;

		if (player.canSee(wx, wy, player.data.z))
			terminal.write(world.glyph(wx, wy, player.data.z), x, y, world.color(wx, wy, player.data.z));
		else
			terminal.write(fov.tile(wx, wy, player.data.z).glyph(), x, y, Color.darkGray);
	}

	/**
	 * Gets the scroll X.
	 *
	 * @param playScreen TODO
	 * @return the scroll X
	 */
	public int getScrollX(PlayScreen playScreen) { return Math.max(0, Math.min(player.data.x - screenWidth / 2, world.width() - screenWidth)); }

	/**
	 * Gets the scroll Y.
	 *
	 * @param playScreen TODO
	 * @return the scroll Y
	 */
	public int getScrollY(PlayScreen playScreen) { return Math.max(0, Math.min(player.data.y - screenHeight / 2, world.height() - screenHeight)); }

	/**
	 * User is trying to exit.
	 *
	 * @param playScreen TODO
	 * @return true, if successful
	 */
	boolean userIsTryingToExit(PlayScreen playScreen){
		return player.data.z == 0 && world.tile(player.data.x, player.data.y, player.data.z) == Tile.STAIRS_UP;
	}
}