package game;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>CreatureDataTest</code> contains tests for the class <code>{@link CreatureData}</code>.
 *
 * @generatedBy CodePro at 5/18/21, 10:48 PM
 * @author tritm
 * @version $Revision: 1.0 $
 */
public class CreatureDataTest {
	/**
	 * Run the CreatureData() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testCreatureData_1()
		throws Exception {

		CreatureData result = new CreatureData();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the int attackValue(Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testAttackValue_1()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = null;
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		int result = fixture.attackValue(creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertEquals(0, result);
	}

	/**
	 * Run the int attackValue(Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testAttackValue_2()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = null;
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		int result = fixture.attackValue(creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertEquals(0, result);
	}

	/**
	 * Run the int defenseValue(Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testDefenseValue_1()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = null;
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		int result = fixture.defenseValue(creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertEquals(0, result);
	}

	/**
	 * Run the int defenseValue(Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testDefenseValue_2()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = null;
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		int result = fixture.defenseValue(creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertEquals(0, result);
	}

	/**
	 * Run the Point extracted(int,int,Point,Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testExtracted_1()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		int wx = 1;
		int wy = 1;
		Point end = new Point(1, 1, 1);
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		Point result = fixture.extracted(wx, wy, end, creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertNotNull(result);
	}

	/**
	 * Run the Point extracted(int,int,Point,Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testExtracted_2()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		int wx = 1;
		int wy = 1;
		Point end = new Point(1, 1, 1);
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		Point result = fixture.extracted(wx, wy, end, creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertNotNull(result);
	}

	/**
	 * Run the Point extracted(int,int,Point,Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testExtracted_3()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		int wx = 1;
		int wy = 1;
		Point end = new Point(1, 1, 1);
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		Point result = fixture.extracted(wx, wy, end, creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertNotNull(result);
	}

	/**
	 * Run the void gainXp(Creature,Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testGainXp_1()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature other = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);
		CreatureData creatureData = new CreatureData();
		creatureData.maxHp = 1;
		other.data = creatureData;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		fixture.gainXp(other, creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	}

	/**
	 * Run the void gainXp(Creature,Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testGainXp_2()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature other = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);
		CreatureData creatureData = new CreatureData();
		creatureData.maxHp = 1;
		other.data = creatureData;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		fixture.gainXp(other, creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	}

	/**
	 * Run the List<Creature> getCreaturesWhoSeeMe() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testGetCreaturesWhoSeeMe_1()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;

		List<Creature> result = fixture.getCreaturesWhoSeeMe();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertNotNull(result);
	}

	/**
	 * Run the List<Creature> getCreaturesWhoSeeMe() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testGetCreaturesWhoSeeMe_2()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;

		List<Creature> result = fixture.getCreaturesWhoSeeMe();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertNotNull(result);
	}

	/**
	 * Run the List<Creature> getCreaturesWhoSeeMe() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testGetCreaturesWhoSeeMe_3()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;

		List<Creature> result = fixture.getCreaturesWhoSeeMe();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: java.lang.ClassCastException@25731dca
		//       at jdk.internal.reflect.GeneratedConstructorAccessor161.newInstance(Unknown Source)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertNotNull(result);
	}

	/**
	 * Run the List<Creature> getCreaturesWhoSeeMe() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testGetCreaturesWhoSeeMe_4()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;

		List<Creature> result = fixture.getCreaturesWhoSeeMe();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: java.lang.ClassCastException@6b081fc6
		//       at jdk.internal.reflect.GeneratedConstructorAccessor161.newInstance(Unknown Source)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertNotNull(result);
	}

	/**
	 * Run the List<Creature> getCreaturesWhoSeeMe() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testGetCreaturesWhoSeeMe_5()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;

		List<Creature> result = fixture.getCreaturesWhoSeeMe();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: java.lang.ClassCastException@132fb9f7
		//       at jdk.internal.reflect.GeneratedConstructorAccessor161.newInstance(Unknown Source)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
		assertNotNull(result);
	}

	/**
	 * Run the void leaveCorpse(Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testLeaveCorpse_1()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		fixture.leaveCorpse(creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	}

	/**
	 * Run the void modifyMana(int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testModifyMana_1()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		int amount = 1;

		fixture.modifyMana(amount);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	}

	/**
	 * Run the void regenerateMana(Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testRegenerateMana_1()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		fixture.regenerateMana(creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	}

	/**
	 * Run the void regenerateMana(Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testRegenerateMana_2()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		fixture.regenerateMana(creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	}

	/**
	 * Run the void regenerateMana(Creature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Test
	public void testRegenerateMana_3()
		throws Exception {
		CreatureData fixture = new CreatureData();
		fixture.ai = new CreatureAi(new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1));
		fixture.inventory = new Inventory(1);
		fixture.weapon = new Item('', new Color(1), "", "");
		fixture.defenseValue = 1;
		fixture.maxHp = 1;
		fixture.y = 1;
		fixture.maxMana = 1;
		fixture.regenHpPer1000 = 1;
		fixture.z = 1;
		fixture.detectCreatures = 1;
		fixture.glyph = '';
		fixture.world = new World(new Tile[][][] {});
		fixture.causeOfDeath = "";
		fixture.name = "";
		fixture.regenManaPer1000 = 1;
		fixture.armor = new Item('', new Color(1), "", "");
		fixture.xp = 1;
		fixture.hp = 1;
		fixture.attackValue = 1;
		fixture.effects = new LinkedList();
		fixture.visionRadius = 1;
		fixture.food = 1;
		fixture.x = 1;
		fixture.level = 1;
		fixture.mana = 1;
		fixture.regenManaCooldown = 1;
		fixture.regenHpCooldown = 1;
		fixture.color = new Color(1);
		fixture.maxFood = 1;
		Creature creature = new Creature(new World(new Tile[][][] {}), '', new Color(1), "", 1, 1, 1);

		fixture.regenerateMana(creature);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: argument type mismatch
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
		//       at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:64)
		//       at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(CreatureDataTest.class);
	}
}