package game;

import java.util.List;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>WorldBuilderDataTest</code> contains tests for the class <code>{@link WorldBuilderData}</code>.
 *
 * @generatedBy CodePro at 5/18/21, 10:47 PM
 * @author tritm
 * @version $Revision: 1.0 $
 */
public class WorldBuilderDataTest {
	/**
	 * Run the WorldBuilderData() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testWorldBuilderData_1()
		throws Exception {

		WorldBuilderData result = new WorldBuilderData();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void connectRegionsDown(int,WorldBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testConnectRegionsDown_1()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		WorldBuilder worldBuilder = new WorldBuilder(1, 1, 1);

		fixture.connectRegionsDown(z, worldBuilder);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void connectRegionsDown(int,WorldBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testConnectRegionsDown_2()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		WorldBuilder worldBuilder = new WorldBuilder(1, 1, 1);

		fixture.connectRegionsDown(z, worldBuilder);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void connectRegionsDown(int,WorldBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testConnectRegionsDown_3()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		WorldBuilder worldBuilder = new WorldBuilder(1, 1, 1);

		fixture.connectRegionsDown(z, worldBuilder);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void connectRegionsDown(int,WorldBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testConnectRegionsDown_4()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		WorldBuilder worldBuilder = new WorldBuilder(1, 1, 1);

		fixture.connectRegionsDown(z, worldBuilder);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void connectRegionsDown(int,WorldBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testConnectRegionsDown_5()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 0;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		WorldBuilder worldBuilder = new WorldBuilder(1, 1, 1);

		fixture.connectRegionsDown(z, worldBuilder);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void connectRegionsDown(int,WorldBuilder) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testConnectRegionsDown_6()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 0;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		WorldBuilder worldBuilder = new WorldBuilder(1, 1, 1);

		fixture.connectRegionsDown(z, worldBuilder);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void connectRegionsDown(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testConnectRegionsDown_7()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		int r1 = 1;
		int r2 = 1;

		fixture.connectRegionsDown(z, r1, r2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void connectRegionsDown(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testConnectRegionsDown_8()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		int r1 = 1;
		int r2 = 1;

		fixture.connectRegionsDown(z, r1, r2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the int fillRegion(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFillRegion_1()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int x = 1;
		int y = 1;
		int z = 1;

		int result = fixture.fillRegion(region, x, y, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertEquals(0, result);
	}

	/**
	 * Run the int fillRegion(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFillRegion_2()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int x = 1;
		int y = 1;
		int z = 1;

		int result = fixture.fillRegion(region, x, y, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertEquals(0, result);
	}

	/**
	 * Run the int fillRegion(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFillRegion_3()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int x = 1;
		int y = 1;
		int z = 1;

		int result = fixture.fillRegion(region, x, y, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertEquals(0, result);
	}

	/**
	 * Run the int fillRegion(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFillRegion_4()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int x = 1;
		int y = 1;
		int z = 1;

		int result = fixture.fillRegion(region, x, y, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertEquals(0, result);
	}

	/**
	 * Run the int fillRegion(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFillRegion_5()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int x = 1;
		int y = 1;
		int z = 1;

		int result = fixture.fillRegion(region, x, y, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertEquals(0, result);
	}

	/**
	 * Run the int fillRegion(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFillRegion_6()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int x = 1;
		int y = 1;
		int z = 1;

		int result = fixture.fillRegion(region, x, y, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertEquals(0, result);
	}

	/**
	 * Run the int fillRegion(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFillRegion_7()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int x = 1;
		int y = 1;
		int z = 1;

		int result = fixture.fillRegion(region, x, y, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertEquals(0, result);
	}

	/**
	 * Run the int fillRegion(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFillRegion_8()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int x = 1;
		int y = 1;
		int z = 1;

		int result = fixture.fillRegion(region, x, y, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertEquals(0, result);
	}

	/**
	 * Run the int fillRegion(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFillRegion_9()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int x = 1;
		int y = 1;
		int z = 1;

		int result = fixture.fillRegion(region, x, y, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertEquals(0, result);
	}

	/**
	 * Run the List<Point> findRegionOverlaps(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFindRegionOverlaps_1()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		int r1 = 1;
		int r2 = 1;

		List<Point> result = fixture.findRegionOverlaps(z, r1, r2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the List<Point> findRegionOverlaps(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFindRegionOverlaps_2()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		int r1 = 1;
		int r2 = 1;

		List<Point> result = fixture.findRegionOverlaps(z, r1, r2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the List<Point> findRegionOverlaps(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFindRegionOverlaps_3()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		int r1 = 1;
		int r2 = 1;

		List<Point> result = fixture.findRegionOverlaps(z, r1, r2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the List<Point> findRegionOverlaps(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFindRegionOverlaps_4()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		int r1 = 1;
		int r2 = 1;

		List<Point> result = fixture.findRegionOverlaps(z, r1, r2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the List<Point> findRegionOverlaps(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFindRegionOverlaps_5()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		int r1 = 1;
		int r2 = 1;

		List<Point> result = fixture.findRegionOverlaps(z, r1, r2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the List<Point> findRegionOverlaps(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFindRegionOverlaps_6()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 0;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		int r1 = 1;
		int r2 = 1;

		List<Point> result = fixture.findRegionOverlaps(z, r1, r2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the List<Point> findRegionOverlaps(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testFindRegionOverlaps_7()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 0;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int z = 1;
		int r1 = 1;
		int r2 = 1;

		List<Point> result = fixture.findRegionOverlaps(z, r1, r2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the void removeRegion(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testRemoveRegion_1()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int z = 1;

		fixture.removeRegion(region, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void removeRegion(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testRemoveRegion_2()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int z = 1;

		fixture.removeRegion(region, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void removeRegion(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testRemoveRegion_3()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 0;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int z = 1;

		fixture.removeRegion(region, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void removeRegion(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testRemoveRegion_4()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 0;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		int region = 1;
		int z = 1;

		fixture.removeRegion(region, z);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_1()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_2()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_3()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_4()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_5()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_6()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_7()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_8()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_9()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_10()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_11()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 0;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_12()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 0;
		fixture.width = 1;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Run the void smoothExtracted(Tile[][][]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Test
	public void testSmoothExtracted_13()
		throws Exception {
		WorldBuilderData fixture = new WorldBuilderData();
		fixture.height = 1;
		fixture.width = 0;
		fixture.nextRegion = 1;
		fixture.tiles = new Tile[][][] {};
		fixture.regions = new int[][][] {};
		fixture.depth = 1;
		Tile[][][] tiles2 = new Tile[][][] {};

		fixture.smoothExtracted(tiles2);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[[Lgame.Tile; field game.WorldBuilderData.tiles to [Lgame.Tile;
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:167)
		//       at java.base/jdk.internal.reflect.UnsafeFieldAccessorImpl.throwSetIllegalArgumentException(UnsafeFieldAccessorImpl.java:171)
		//       at java.base/jdk.internal.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:81)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 5/18/21, 10:47 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(WorldBuilderDataTest.class);
	}
}