package game;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all
 * of the tests within its package as well as within any subpackages of its
 * package.
 *
 * @generatedBy CodePro at 5/18/21, 10:48 PM
 * @author tritm
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	LevelUpOptionTest.class,
	WorldTest.class,
	EatScreenTest.class,
	PointTest.class,
	WorldBuilderTest.class,
	ZombieAiTest.class,
	CreatureAiDataTest.class,
	FungusAiTest.class,
	TileTest.class,
	LineTest.class,
	BatAiTest.class,
	WorldBuilderDataTest.class,
	LevelUpControllerTest.class,
	FieldOfViewTest.class,
	CreatureTest.class,
	SpellTest.class,
	StuffFactoryTest.class,
	CreatureDataTest.class,
	GoblinAiTest.class,
	ItemTest.class,
	InventoryTest.class,
})
public class TestAll {

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 5/18/21, 10:48 PM
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
